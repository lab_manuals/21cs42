import java.util.ArrayList;
import java.util.List;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class QuickSort {
    private List<Integer> numList;

    public QuickSort() {
        numList = new ArrayList<>();
    }

    public void fnGenRandArray(int n) {
        int i, iVal;
        for (i = 0; i < n; i++) {
            iVal = (int) (Math.random() * 10000);
            numList.add(iVal);
        }
    }

    public void fnDispArray(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(String.format("%8d", numList.get(i)));
        }
    }

    public void fnSortArray(int l, int r) {
        if (l < r) {
            int s = fnPartition(l, r);
            fnSortArray(l, s - 1);
            fnSortArray(s + 1, r);
        }
    }

    public int fnPartition(int l, int r) {
        int pivot = numList.get(l);
        int i = l + 1;
        int j = r;
        while (i <= j) {
            if (numList.get(i) <= pivot) {
                i++;
            } else if (numList.get(j) > pivot) {
                j--;
            } else {
                int temp = numList.get(i);
                numList.set(i, numList.get(j));
                numList.set(j, temp);
                i++;
                j--;
            }
        }
        int temp = numList.get(l);
        numList.set(l, numList.get(j));
        numList.set(j, temp);
        return j;
    }

    public static void main(String[] args) {
        long startTime, endTime;
        int iChoice, iNum;
        QuickSort myListObj = new QuickSort();

        try (PrintWriter fout = new PrintWriter(new FileWriter("QuickPlot.dat"))) {
            for (;;) {
                System.out.println("\n1.Quick Sort\n2.Plot the Graph\n3.Exit");
                System.out.println("Enter your choice");
                iChoice = Integer.parseInt(System.console().readLine());

                switch (iChoice) {
                    case 1:
                        System.out.print("Enter number of elements to sort : ");
                        iNum = Integer.parseInt(System.console().readLine());
                        myListObj.fnGenRandArray(iNum);

                        System.out.println("\nUnsorted Array");
                        myListObj.fnDispArray(iNum);

                        myListObj.fnSortArray(0, iNum - 1);

                        System.out.println("\nSorted Array");
                        myListObj.fnDispArray(iNum);
                        break;
                    case 2:
                        for (int i = 100; i < 100000; i += 100) {
                            myListObj.fnGenRandArray(i);

                            startTime = System.nanoTime();
                            myListObj.fnSortArray(0, i - 1);
                            endTime = System.nanoTime();

                            fout.println(i + "\t" + ((endTime - startTime) / 1e9));
                        }

                        System.out.println("\nData File generated and stored in file < QuickPlot.dat >. Use a plotting utility");
                        break;
                    case 3:
                        System.exit(0);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}

