import java.util.ArrayList;
import java.util.List;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MergeSort {
    private List<Integer> numList;

    public MergeSort() {
        numList = new ArrayList<>();
    }

    public void fnGenRandArray(int n) {
        int i, iVal;
        for (i = 0; i < n; i++) {
            iVal = (int) (Math.random() * 10000);
            numList.add(iVal);
        }
    }

    public void fnDispArray(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(String.format("%8d", numList.get(i)));
        }
    }

    public void fnSortArray(int low, int high) {
        if (low < high) {
            int mid = (low + high) / 2;
            fnSortArray(low, mid);
            fnSortArray(mid + 1, high);
            fnMerge(low, mid, high);
        }
    }

    public void fnMerge(int low, int mid, int high) {
        List<Integer> b = new ArrayList<>();
        int i = low;
        int j = mid + 1;
        int k = 0;
        while (i <= mid && j <= high) {
            if (numList.get(i) < numList.get(j))
                b.add(numList.get(i++));
            else
                b.add(numList.get(j++));
        }
        while (i <= mid)
            b.add(numList.get(i++));
        while (j <= high)
            b.add(numList.get(j++));
        for (i = low; i <= high; i++) {
            numList.set(i, b.get(k));
            k++;
        }
    }

    public static void main(String[] args) {
        long startTime, endTime;
        int iChoice, iNum;
        MergeSort myListObj = new MergeSort();

        try (PrintWriter fout = new PrintWriter(new FileWriter("MergePlot.dat"))) {
            for (;;) {
                System.out.println("\n1.MergeSort\n2.Plot the Graph\n3.Exit");
                System.out.println("Enter your choice");
                iChoice = Integer.parseInt(System.console().readLine());

                switch (iChoice) {
                    case 1:
                        System.out.print("Enter number of elements to sort : ");
                        iNum = Integer.parseInt(System.console().readLine());
                        myListObj.fnGenRandArray(iNum);

                        System.out.println("\nUnsorted Array");
                        myListObj.fnDispArray(iNum);

                        myListObj.fnSortArray(0, iNum - 1);

                        System.out.println("\nSorted Array");
                        myListObj.fnDispArray(iNum);
                        break;
                    case 2:
                        for (int i = 100; i < 100000; i += 100) {
                            myListObj.fnGenRandArray(i);

                            startTime = System.nanoTime();
                            myListObj.fnSortArray(0, i - 1);
                            endTime = System.nanoTime();

                            fout.println(i + "\t" + ((endTime - startTime) / 1e9));
                        }

                        System.out.println("\nData File generated and stored in file < MergePlot.dat >. Use a plotting utility");
                        break;
                    case 3:
                        System.exit(0);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}

