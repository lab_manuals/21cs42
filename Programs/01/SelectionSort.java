import java.util.ArrayList;
import java.util.List;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SelectionSort {
    private List<Integer> numList;

    public SelectionSort() {
        numList = new ArrayList<>();
    }

    public void fnGenRandArray(int n) {
        int i, iVal;
        for (i = 0; i < n; i++) {
            iVal = (int) (Math.random() * 10000);
            numList.add(iVal);
        }
    }

    public void fnDispArray(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(String.format("%8d", numList.get(i)));
        }
    }

    public void fnSortArray(int n) {
        int i, j, min_idx;
        for (i = 0; i < n - 1; i++) {
            min_idx = i;
            for (j = i + 1; j < n; j++) {
                if (numList.get(j) < numList.get(min_idx))
                    min_idx = j;
            }
            fnSwap(i, min_idx);
        }
    }

    public void fnSwap(int a, int b) {
        int temp = numList.get(a);
        numList.set(a, numList.get(b));
        numList.set(b, temp);
    }

    public static void main(String[] args) {
        long startTime, endTime;
        int iChoice, iNum;
        SelectionSort myListObj = new SelectionSort();

        try (PrintWriter fout = new PrintWriter(new FileWriter("SelectPlot.dat"))) {
            for (;;) {
                System.out.println("\n1.SelectionSort\n2.Plot the Graph\n3.Exit");
                System.out.println("Enter your choice");
                iChoice = Integer.parseInt(System.console().readLine());

                switch (iChoice) {
                    case 1:
                        System.out.print("Enter number of elements to sort : ");
                        iNum = Integer.parseInt(System.console().readLine());
                        myListObj.fnGenRandArray(iNum);

                        System.out.println("\nUnsorted Array");
                        myListObj.fnDispArray(iNum);

                        myListObj.fnSortArray(iNum);

                        System.out.println("\nSorted Array");
                        myListObj.fnDispArray(iNum);
                        break;
                    case 2:
                        for (int i = 100; i < 10000; i += 100) {
                            myListObj.fnGenRandArray(i);

                            startTime = System.nanoTime();
                            myListObj.fnSortArray(i);
                            endTime = System.nanoTime();
//							System.out.println(i + "\t" + (endTime - startTime));
                            fout.println(i + "\t" + ((endTime - startTime) / 1e9));
                        }

                        System.out.println("\nData File generated and stored in file < SelectPlot.dat >. Use a plotting utility");
                        break;
                    case 3:
                        System.exit(0);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}

