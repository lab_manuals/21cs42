import java.util.Scanner;

class Kruskal {
    static class Edge {
        int u, v, cost;
    }

    private static final int MAXNODES = 10;
    private static final int INF = 9999;

    private static int findParent(int v, int[] parent) {
        while (parent[v] != v)
            v = parent[v];
        return v;
    }

    private static void union(int i, int j, int[] parent) {
        if (i < j)
            parent[j] = i;
        else
            parent[i] = j;
    }

    private static void inputGraph(int m, Edge[] e) {
        Scanner scanner = new Scanner(System.in);
        for (int k = 0; k < m; k++) {
            System.out.println("Enter edge and cost in the form u, v, w :");
            int i = scanner.nextInt();
            int j = scanner.nextInt();
            int cost = scanner.nextInt();
            e[k] = new Edge();
            e[k].u = i;
            e[k].v = j;
            e[k].cost = cost;
        }
        scanner.close();
    }

    private static int getMinEdge(Edge[] e, int n) {
        int small = INF;
        int pos = -1;
        for (int i = 0; i < n; i++) {
            if (e[i].cost < small) {
                small = e[i].cost;
                pos = i;
            }
        }
        return pos;
    }

    private static void kruskal(int n, Edge[] e, int m) {
        int count = 0;
        int k = 0;
        int sum = 0;
        int[][] t = new int[MAXNODES][2];
        int[] parent = new int[MAXNODES];

        for (int i = 0; i < n; i++) {
            parent[i] = i;
        }

        while (count != n - 1) {
            int pos = getMinEdge(e, m);
            if (pos == -1) {
                break;
            }
            int u = e[pos].u;
            int v = e[pos].v;
            int i = findParent(u, parent);
            int j = findParent(v, parent);
            if (i != j) {
                t[k][0] = u;
                t[k][1] = v;
                k++;
                count++;
                sum += e[pos].cost;
                union(i, j, parent);
            }
            e[pos].cost = INF;
        }

        if (count == n - 1) {
            System.out.println("\nSpanning tree exists");
            System.out.println("The Spanning tree is shown below");
            for (int i = 0; i < n - 1; i++)
                System.out.println(t[i][0] + " " + t[i][1]);
            System.out.println("Cost of the spanning tree: " + sum);
        } else {
            System.out.println("\nThe spanning tree does not exist");
        }
    }

    public static void main(String[] args) {
        int n = 6, m = 20;
        Edge[] e = new Edge[2 * MAXNODES];
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of nodes : ");
        n = scanner.nextInt();
        System.out.print("Enter the number of edges : ");
        m = scanner.nextInt();
        inputGraph(m, e);
        kruskal(n, e, m);
    }
}

