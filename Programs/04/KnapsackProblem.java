import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

class Item {
    int profit;
    int weight;
    float profitRatio;
    float fraction;
}

public class KnapsackProblem {
    public static void main(String[] args) {
        List<Item> itemList = new ArrayList<>();
        int iNum, i;
        float knCap, remCap, totProfit;
        Item a;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of items: ");
        iNum = scanner.nextInt();

        System.out.print("Enter Knapsack capacity: ");
        knCap = scanner.nextFloat();

        for (i = 0; i < iNum; i++) {
            a = new Item();

            System.out.print("Enter profit: ");
            a.profit = scanner.nextInt();

            System.out.print("Enter weight: ");
            a.weight = scanner.nextInt();

            a.profitRatio = (float) a.profit / a.weight;
            a.fraction = 0.0f;

            itemList.add(a);
        }

        itemList.sort(Comparator.comparing((Item item) -> item.profitRatio).reversed());

        remCap = knCap;
        totProfit = 0.0f;

        for (i = 0; i < iNum; i++) {
            a = itemList.get(i);

            if (a.weight < remCap) {
                a.fraction = 1.0f;
                remCap -= a.weight;
                totProfit += a.profit;
            } else {
                a.fraction = remCap / a.weight;
                remCap = 0;
                totProfit += a.profit * a.fraction;
                break;
            }
        }

        System.out.println("\nKNAPSACK SOLUTION");
        System.out.println("Item\tWeight\tProfit\tFraction Chosen");

        for (i = 0; i < iNum; i++) {
            a = itemList.get(i);
            System.out.printf("%d\t%d\t%d\t%.2f\n", i + 1, a.weight, a.profit, a.fraction);
        }

        System.out.println("\nTotal Profit Earned: " + totProfit);

        scanner.close();
    }
}

